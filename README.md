# parallel-image-viewer

## Keybinding configuration

Run the application with `--default-keybindings` to see the default keybindings configuration (in JSON).
The program seeks the configuration in the following places:
 1. If `-k` option is given then it uses the file pointed by -k option
 2. If the xdg user config dir contains path `parallel-image-viewer/keybindings.json` it uses this file.
 3. If the xdg system config dir contains path `parallel-image-viewer/keybindings.json` it uses this file.
 4. Otherwise uses the default configuration as provided by `--default-feybindings`

# Author
Jan Karwowski: jan_karwowski@yahoo.com
