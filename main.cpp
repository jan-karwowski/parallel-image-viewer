#include <cstddef>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <memory>

#include <wx/cmdline.h>
#include <wx/stdpaths.h>
#include <wx/wx.h>

#include "image_set.hpp"
#include "keybindings_json.hpp"
#include "list_image_set.hpp"
#include "parallel_image_viewer_window.hpp"

#define FILE_LISTS_SEPARATOR "++"

namespace piv::configuration {
static const inline std::string program_config_dir = "parallel-image-viewer";
static const inline std::string keybindings_config = "keybindings.json";
} // namespace piv::configuration

namespace piv {

static const inline keybindings::keyboard_config default_bindings{
    .bindings = {
        {.action = keybindings::actions::pan<
             keybindings::actions::direction::HORIZONTAL>{5},
         .key = 'l'},
        {.action = keybindings::actions::pan<
             keybindings::actions::direction::HORIZONTAL>{-5},
         .key = 'h'},
        {.action = keybindings::actions::pan<
             keybindings::actions::direction::VERTICAL>{5},
         .key = 'k'},
        {.action = keybindings::actions::pan<
             keybindings::actions::direction::VERTICAL>{-5},
         .key = 'j'},
        {.action = keybindings::actions::zoom{.1}, .key = '+'},
        {.action = keybindings::actions::zoom{-.1}, .key = '-'},
        {.action = keybindings::actions::async_mode<0>(), .key = '1'},
        {.action = keybindings::actions::async_mode<1>(), .key = '2'},
        {.action = keybindings::actions::sync_mode(), .key = '`'},
        {.action = keybindings::actions::next_image(), .key = 'f'},
        {.action = keybindings::actions::prev_image(), .key = 'b'},
        {.action = keybindings::actions::quit(), .key = 'q'},
	{.action = keybindings::actions::keyboard_help(), .key = '?'},
        {.action = keybindings::actions::async_viewport_mode<0>(), .key = 's'},
        {.action = keybindings::actions::async_viewport_mode<1>(), .key = 'd'},
        {.action = keybindings::actions::sync_viewport_mode(), .key = 'a'}}};

class parallel_image_viewer : public wxApp {
  std::unique_ptr<filesystem::image_set> imset;
  std::optional<std::filesystem::path> config_file_path;

  static constexpr wxCmdLineEntryDesc g_cmdLineDesc[] = {
      {wxCMD_LINE_SWITCH, "h", "help",
       "displays help on the command line parameters", wxCMD_LINE_VAL_NONE,
       wxCMD_LINE_OPTION_HELP},
      {wxCMD_LINE_OPTION, "1", "first-dir", "the first image dir",
       wxCMD_LINE_VAL_STRING, 0},
      {wxCMD_LINE_OPTION, "2", "second-dir", "the second image dir",
       wxCMD_LINE_VAL_STRING, 0},
      {wxCMD_LINE_SWITCH, nullptr, "default-keybindings",
       "dump the default keybindings config", wxCMD_LINE_VAL_NONE, 0},
      {wxCMD_LINE_OPTION, "k", "keybindings",
       "specify the keybinding config file (overrides system and user "
       "locations)",
       wxCMD_LINE_VAL_STRING, wxCMD_LINE_PARAM_OPTIONAL},
      {wxCMD_LINE_PARAM, nullptr, nullptr,
       "a list of files to display. First all files for the first display, "
       "then " FILE_LISTS_SEPARATOR
       " separator and then all files for the second display.",
       wxCMD_LINE_VAL_STRING,
       wxCMD_LINE_PARAM_MULTIPLE | wxCMD_LINE_PARAM_OPTIONAL},
      {wxCMD_LINE_NONE, nullptr, nullptr, nullptr, wxCMD_LINE_VAL_NONE, 0}};

public:
  void try_find_keyboard_config() {
    auto paths = wxStandardPaths::Get();
    paths.SetFileLayout(wxStandardPathsBase::FileLayout_XDG);
    if (!config_file_path) {
      std::filesystem::path dir = paths.GetUserConfigDir().utf8_string();
      auto config_path = dir / piv::configuration::program_config_dir /
                         piv::configuration::keybindings_config;
      if (std::filesystem::exists(config_path)) {
        config_file_path = config_path;
      }
    }
    if (!config_file_path) {
      std::filesystem::path dir = paths.GetConfigDir().utf8_string();
      auto config_path = dir / piv::configuration::program_config_dir /
                         piv::configuration::keybindings_config;
      if (std::filesystem::exists(config_path)) {
        config_file_path = config_path;
      }
    }
  }
  bool OnInit() override {
    if (!wxApp::OnInit())
      return false;
    try_find_keyboard_config();
    wxInitAllImageHandlers();

    keybindings::keyboard_config bindings;
    if (config_file_path) {
      nlohmann::json j;
      try {
        {
          std::ifstream input;
          input.exceptions(std::ifstream::failbit | std::ifstream::badbit);
          input.open(config_file_path.value());
          input >> j;
        }
        bindings = j;
      } catch (std::exception &e) {
        std::cerr << e.what();
        return false;
      }
    } else {
      bindings = default_bindings;
    }
    piv::window::parallel_image_viewer_window *win =
        new piv::window::parallel_image_viewer_window(std::move(imset),
                                                      bindings);
    win->Show();
    return true;
  }

  void OnInitCmdLine(wxCmdLineParser &parser) override {
    parser.SetDesc(g_cmdLineDesc);
  }

  bool OnCmdLineParsed(wxCmdLineParser &parser) override {
    wxString dir1;
    wxString dir2;
    bool dir1_parsed = parser.Found("1", &dir1);
    bool dir2_parsed = parser.Found("2", &dir2);
    wxString keys_path;
    if (parser.Found("k", &keys_path)) {
      config_file_path = keys_path.utf8_string();
    }
    if (parser.Found("default-keybindings")) {
      nlohmann::json j = default_bindings;
      std::wcout << j.dump(2) << "\n";
      return false;
    } else if (!dir1_parsed != !dir2_parsed) {
      std::wcerr << "Both -1 and -2 options must be specified!\n";
      return false;
    } else if (dir1_parsed && dir2_parsed && parser.GetParamCount() > 0) {
      std::wcerr << "-1 and -2 options specified together with positional "
                    "agruments. "
                    "Either dir options or a file list mus be used\n";
      return false;
    } else if (dir1_parsed && dir2_parsed) {
      std::filesystem::path d1 = dir1.ToStdString();
      std::filesystem::path d2 = dir2.ToStdString();
      imset = std::unique_ptr<filesystem::image_set>(
          new filesystem::file_path_matching_image_set(
              d1, d2, {".jpg", ".jpeg", ".png"}, true));
      return true;
    } else {
      std::vector<std::filesystem::path> files1;
      std::vector<std::filesystem::path> files2;
      bool found_separator = false;
      for (size_t i = 0; i < parser.GetParamCount(); i++) {
        wxString param = parser.GetParam(i);
        // TODO detect that separator was given twice?
        if (found_separator) {
          files2.push_back(param.ToStdString());
        } else if (param == wxString("++")) {
          found_separator = true;
        } else {
          files1.push_back(param.ToStdString());
        }
      }
      if (found_separator) {
        imset = std::unique_ptr<filesystem::image_set>(
            new filesystem::list_image_set(std::move(files1),
                                           std::move(files2)));
        return true;
      } else {
        std::wcerr << "Two file vectors separated by " FILE_LISTS_SEPARATOR
                      " must be supplied.\n";
        return false;
      }
    }

    return true;
  }
};
} // namespace piv

wxIMPLEMENT_APP(piv::parallel_image_viewer);
