% parallel-image-viewer(1) | compare pairs of image files side-by-side

# Name
`parallell-image-viewer` -- Open two sets of image files and and compare each pair of files (one from the one set and the other one from the second one) in a split-screen window. Zoom and pan images simultaneously or switch to separate viewport for each side.

# Synopsis
`parallel-image-viewer -1 dirA -2 dirB [-k bindings.json]` --- compare two directories  
`parallel-image-viewer [-k bindings.json] fileA1.jpg fileA2.jpg ... ++ fileB1.jpg fileB2.jpg ...` --- compare explicitly given list of files  
`parallel-image-viewer -h` --- display help message  
`parallel-image-viewer --default-keybindings` --- print the default keybindings file

# Description

Parallel image viewer is built around the concept of a set of pairs of files (file *A* and file *B*). Typical use case is that a user has a two sets of the same images compressed with different algorithm and wants to compare visual quality of images. In such case image *A* is an image compressed with algo1 and *B* is compressed with algo2. Another use case is that the user has two sets of plots being a result of a scientific experiment. One set comes from one parametrization of the experiment and the other from another parametrization. The user wants to see how the change of parameters influence the results of experiment and compare the respective plots side-by-side.

`parallel-image-viewer` provides the user with a program window that contains twin image viewing areas. The program is called with a definition of a vector of image pairs. The image views display the respective images from each pair. All operations are done in sync in both image displays: moving to next/previous image causes that both views are updated with the new image, panning and zooming image is also done on both views simultaneously. This is the default behavior and it is called **sync mode**. 

The **sync mode** can be changed to **async mode**, separetely for:

 - position, so the image on the left can be from the other pair than in the right one. (in such case the user can change either position in left or right panel),
 - viewport, so the images on the left and right can be zoomed and panned separately.

## Program options

`-1 dir` `--first-dir dir`

: the path of the first directory containing image files (see Reading image pairs)

`-2 dir` `--second-dir dir`

: the path of the first directory containing image files (see Reading image pairs)

`-k path/to.json` `--keybindings path/to.json`

: path to the keyboard bindings configuration file (see Configuration files for more details)

`--default-keybindings`

: prints the default keybindings config file to stdout and exits

`-h` `--help`

: prints the commandline usage help

## Reading image pairs

There are two methods to read pairs of images into the program: 

The first one is to use `-1 dirA` and `-2 dirB` options (both options must be given). If the program was called with those two options, then *dirA* and *dirB* are scanned for all image files. The files that have the same relative paths in both directories (possibly differing with the file extensions) are grouped in pairs. The files that do not have paired image in the other directory will be displayed with the other view empty.

The second approach is not to specify `-1` and `-2` options, but instead give the program list of image files as trailing arguments. The list must contain exactly one `++` argument which is the separator. The files before the separator are the first vector and the files after the separator are the second vector. The first file from the first vector is paired with the first file from the second vector, the second file from the first vector is paired with the second file form the second vector and so on. 

## Sync mode, async mode and focus

The program is driven by keyboard commands. When in sync mode the commands apply to both image views. When in async mode, the commands apply only to the focused image. The current mode is provided in the status line at the bottom of the image. The mode can have one of three values: 
 - *SYNC* -- both views operate in sync mode,
 - *ACTIVE* -- views are in async mode and this one is focused and accepts commands,
 - *INACTIVE* -- views are in async mode and the other view accepts commands

The mode is specified twice, with a letter in parentheses following it: (P)osition -- the position in image list (next/prev image commands) and (V)ievport commands (zoom and pan). For each of those the async/sync mode is specified separately. Similarly the focus. One can set viewport focus to left image and position focus to the right image.

## Program usage

`parallel-image-viewer` is a keyboard-driven GUI application. The following commands are available:
  - pan horizontally by number of pixels,
  - pan vertically by number of pixels,
  - zoom by specific amount (positive amount = zoom in, negative = zoom out),
  - quit program
  - show current keybindings
  - switch to sync position mode
  - switch to async position mode (if not already) and move focus to the left view
  - switch to async position mode (if not already) and move focus to the right view
  - switch to sync viewport mode
  - switch to async viewport mode (if not already) and move focus to the left view
  - switch to async viewport mode (if not already) and move focus to the right view

Switching back to sync mode synchronizes the viewport (respectively position) to the currently focused image.

## Default keybindings

Please not that keybindings are case sensitive and that any characters on the same key (like + and = on QWERTY keyboard) are distinct keybindings.

The default keybindings are:
 - **l** pan 5px right
 - **h** pan 5px left
 - **j** pan 5px bottom
 - **k** pan 5px top
 - **+** zoom in by 0.1
 - **-** zoom out by 0.1
 - **f** move **f**orward (to the next image)
 - **b** move **b**ackward (to the previous image)
 - **1** switch to async position mode and move focus to the left view
 - **2** switch to async position mode and move focus to the right view
 - **\`** switch to sync position mode
 - **s** switch to async viewport mode and move focus to the left view
 - **d** switch to async viewport mode and move focus to the right view
 - **a** switch to sync viewport mode
 - **q** quits program
 - **?** displays the help windows describing all the keybindings currently configured 

# Program window layout
The program window consists of five parts:
 - left image view
 - left image status bar (shows sync/async modes and image path)
 - right image view
 - right image status bar (shows sync/async modes and image path)
 - main status bar (under images' status bars) -- displays messages from the program.
# Configuration files

Currently only keybindings can be changed by a configuration file. The configuration file uses JSON format. The default file can be obtained by calling the program with the respective parameter. 

**TODO** provide a description of the config file format

A command can be bound to any number of keys. Currently it is not possible to bind special keys like arrows of function keys (some day I will add this feature). 
*Binding several commands to a single key produces undefined behavior. (In the future the program will check this and this will be illegal file).*

## Keybindings 

The program seeks the configuration in the following places:
 1. If `-k` option is given then it uses the file pointed by -k option
 2. If the xdg user config dir contains path `parallel-image-viewer/keybindings.json` it uses this file.
 3. If the xdg system config dir contains path `parallel-image-viewer/keybindings.json` it uses this file.
 4. Otherwise uses the default configuration as provided by `--default-keybindings`

# Author

Jan Karwowski (jan_karwowski@yahoo.com)
