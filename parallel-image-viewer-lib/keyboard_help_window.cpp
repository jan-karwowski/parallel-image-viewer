#include "keyboard_help_window.hpp"
#include "keybindings_description.hpp"

namespace piv::help {
keyboard_widget::keyboard_widget(wxWindow *parent, wxWindowID id,
                                 const keybindings::keyboard_config &keys)
    : wxPanel(parent, id) {
  wxFlexGridSizer *sizer = new wxFlexGridSizer(3);
  SetSizer(sizer);

  for (const keybindings::binding &bin : keys.bindings) {
    wxStaticText *key = new wxStaticText(this, wxID_ANY, bin.key);
    wxStaticText *description;
    std::visit(
        [this, &description](auto &&action) {
          description = new wxStaticText(
              this, wxID_ANY,
              keybindings::actions::describe<std::decay_t<decltype(action)>>()(
                  action));
        },
        bin.action);
    sizer->Add(key);
    sizer->AddSpacer(4);
    sizer->Add(description, 1, wxGROW);
  }
}

keyboard_window::keyboard_window(wxWindow *parent, wxWindowID id,
                                 const keybindings::keyboard_config &keys)
    : wxFrame(parent, id, "Keyboard help") {
  keyboard_widget *main_widget = new keyboard_widget(this, wxID_ANY, keys);
  auto close_button = new wxButton(this, wxID_ANY, "Close help window");
  auto description_text =
      new wxStaticText(this, wxID_ANY, "Current keybindings:");
  auto sizer = new wxBoxSizer(wxVERTICAL);
  sizer->Add(description_text);
  sizer->Add(main_widget);
  sizer->AddStretchSpacer();
  sizer->Add(close_button);
  SetSizer(sizer);
  close_button->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
                     [this](auto &&action) { Hide(); });
  Bind(wxEVT_CLOSE_WINDOW, [this](wxCloseEvent &evt) {
    if (evt.CanVeto()) {
      evt.Veto();
      Hide();
    } else {
      std::cerr << "NOT IMPLEMENTED\n";
    }
  });
}
} // namespace piv::help
