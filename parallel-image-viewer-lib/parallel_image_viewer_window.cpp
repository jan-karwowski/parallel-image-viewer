#include "parallel_image_viewer_window.hpp"

namespace piv::window {

namespace browsing_mode {

template <unsigned long I> struct update {
  static constexpr auto which = I;
  std::optional<std::filesystem::path> new_path;
};

template <class mode> struct mode_ops {
  auto forward(mode &) const = delete;
  auto backward(mode &) const = delete;
};

template <class ModeFrom, class ModeTo> struct mode_switcher {
  ModeTo operator()(const ModeFrom &&) = delete;
};

template <> struct mode_ops<sync_mode> {
  std::variant<std::tuple<>, std::tuple<update<0>, update<1>>>
  forward(sync_mode &mode) const {
    if (mode.iterator->forward()) {
      auto [first, second] = mode.iterator->current_images();
      return std::make_tuple(update<0>{std::move(first)},
                             update<1>{std::move(second)});
    } else {
      return std::tuple<>();
    }
  }

  std::variant<std::tuple<>, std::tuple<update<0>, update<1>>>
  backward(sync_mode &mode) const {
    if (mode.iterator->backward()) {
      auto [first, second] = mode.iterator->current_images();
      return std::make_tuple(update<0>{std::move(first)},
                             update<1>{std::move(second)});
    } else {
      return std::tuple<>();
    }
  }
};

template <unsigned long I> struct mode_ops<async_mode<I>> {
  std::variant<std::tuple<>, std::tuple<update<I>>>
  forward(async_mode<I> &mode) const {
    if (std::get<I>(mode.iterators)->forward()) {
      std::wcout << "The path: " << I << " "
                 << std::get<I>(mode.iterators)->current_image().value()
                 << "\n"; // TODO remove
      return std::make_tuple(
          update<I>{std::get<I>(mode.iterators)->current_image()});
    } else {
      return std::tuple<>();
    }
  }

  std::variant<std::tuple<>, std::tuple<update<I>>>
  backward(async_mode<I> &mode) const {
    if (std::get<I>(mode.iterators)->backward()) {
      return std::make_tuple(
          update<I>{std::get<I>(mode.iterators)->current_image()});
    } else {
      return std::tuple<>();
    }
  }
};

template <unsigned long I, unsigned long J>
struct mode_switcher<async_mode<I>, async_mode<J>> {
  async_mode<J> operator()(async_mode<I> &&from) const {
    return async_mode<J>{std::move(from.iterators)};
  }
};

template <> struct mode_switcher<sync_mode, sync_mode> {
  sync_mode operator()(sync_mode &&from) const { return std::move(from); }
};

template <unsigned long I> struct mode_switcher<sync_mode, async_mode<I>> {
  async_mode<I> operator()(sync_mode &&from) const {
    return async_mode<I>{from.iterator->to_async()};
  }
};

template <unsigned long I> struct mode_switcher<async_mode<I>, sync_mode> {
  sync_mode operator()(async_mode<I> &&from) {
    return sync_mode{std::get<I>(from.iterators)->to_sync()};
  }
};
} // namespace browsing_mode

namespace perspectives {
template <class PerspectiveMode> struct get_perspective {
  image_perspective &operator()(PerspectiveMode &) const = delete;
};

template <> struct get_perspective<sync_mode> {
  image_perspective &operator()(sync_mode &mode) const {
    return mode.perspective;
  }
};

template <unsigned long I> struct get_perspective<async_mode<I>> {
  image_perspective &operator()(async_mode<I> &mode) const {
    return std::get<I>(mode.perspectives);
  }
};

template <class Mode, unsigned long L> struct update_images {
  void operator()(const Mode &, std::array<image_view *, L> &) const = delete;
};

template <unsigned long L> struct update_images<sync_mode, L> {
  template <unsigned long N, unsigned long... Indices>
  void update_image(const image_perspective &per,
                    std::array<image_view *, L> &images,
                    std::integer_sequence<unsigned long, Indices...>) const {
    (std::get<Indices>(images)->set_perspective(per), ...);
  }

  void operator()(const sync_mode &mode,
                  std::array<image_view *, L> &array) const {
    update_image<L>(mode.perspective, array,
                    std::make_integer_sequence<unsigned long, L>());
  }
};

template <unsigned long I, unsigned long L>
struct update_images<async_mode<I>, L> {
  void operator()(const async_mode<I> &mode,
                  std::array<image_view *, L> &array) const {
    std::get<I>(array)->set_perspective(std::get<I>(mode.perspectives));
  }
};

template <class Mode1, class Mode2> struct mode_switcher {
  Mode2 operator()(const Mode1 &mode) const = delete;
};

template <> struct mode_switcher<sync_mode, sync_mode> {
  sync_mode operator()(const sync_mode &mode) { return mode; }
};

template <unsigned long From, unsigned long To>
struct mode_switcher<async_mode<From>, async_mode<To>> {
  async_mode<To> operator()(const async_mode<From> &mode) {
    return async_mode<To>{mode.perspectives};
  }
};

template <unsigned long To> struct mode_switcher<sync_mode, async_mode<To>> {
  async_mode<To> operator()(const sync_mode &mode) {
    return async_mode<To>{
        std::array<image_perspective, 2>{mode.perspective, mode.perspective}};
  }
};

template <unsigned long From>
struct mode_switcher<async_mode<From>, sync_mode> {
  sync_mode operator()(const async_mode<From> &mode) {
    return sync_mode{std::get<From>(mode.perspectives)};
  }
};

} // namespace perspectives

template <>
struct image_viewer_action_handler<keybindings::actions::next_image> {
  void operator()(const keybindings::actions::next_image &,
                  parallel_image_viewer_window &window) const {
    window.next_img();
  }
};
template <>
struct image_viewer_action_handler<keybindings::actions::prev_image> {
  void operator()(const keybindings::actions::prev_image &,
                  parallel_image_viewer_window &window) const {
    window.prev_img();
  }
};
template <unsigned int I>
struct image_viewer_action_handler<keybindings::actions::async_mode<I>> {
  void operator()(const keybindings::actions::async_mode<I> &,
                  parallel_image_viewer_window &window) const {
    window.switch_to_async_mode<I>();
  }
};
template <>
struct image_viewer_action_handler<keybindings::actions::sync_mode> {
  void operator()(const keybindings::actions::sync_mode &,
                  parallel_image_viewer_window &window) const {
    window.switch_to_sync_mode();
  }
};
template <keybindings::actions::direction D>
struct image_viewer_action_handler<keybindings::actions::pan<D>> {
  void operator()(const keybindings::actions::pan<D> &pan,
                  parallel_image_viewer_window &window) const {
    std::visit(
        [&pan, &window](auto &&mode) {
          image_perspective &perspective =
              perspectives::get_perspective<std::decay_t<decltype(mode)>>()(
                  mode);
          if constexpr (D == keybindings::actions::direction::HORIZONTAL) {
            perspective.centerShiftX += pan.pixels;
          } else {
            perspective.centerShiftY += pan.pixels;
          }

          perspectives::update_images<std::decay_t<decltype(mode)>, 2UL>()(
              mode, window.imgs);
        },
        window.perspective);
  }
};
template <> struct image_viewer_action_handler<keybindings::actions::zoom> {
  void operator()(const keybindings::actions::zoom &z,
                  parallel_image_viewer_window &window) const {
    std::visit(
        [&z, &window](auto &&mode) {
          image_perspective &perspective =
              perspectives::get_perspective<std::decay_t<decltype(mode)>>()(
                  mode);
          perspective.zoomX += z.amount;
          perspective.zoomY += z.amount;

          perspectives::update_images<std::decay_t<decltype(mode)>, 2UL>()(
              mode, window.imgs);
        },
        window.perspective);
  }
};
template <> struct image_viewer_action_handler<keybindings::actions::quit> {
  void operator()(const keybindings::actions::quit &,
                  parallel_image_viewer_window &window) const {
    window.Close();
  }
};

template <>
struct image_viewer_action_handler<keybindings::actions::sync_viewport_mode> {
  void operator()(const keybindings::actions::sync_viewport_mode &,
                  parallel_image_viewer_window &window) const {
    window.perspective = std::visit(
        [&window](auto &&current_mode) {
          auto new_mode =
              perspectives::mode_switcher<std::decay_t<decltype(current_mode)>,
                                          perspectives::sync_mode>()(
                  current_mode);
          perspectives::update_images<perspectives::sync_mode, 2UL>()(
              new_mode, window.imgs);
          return new_mode;
        },
        window.perspective);
    std::get<0>(window.imgs)->switch_viewport_state(image_view_state::SYNC);
    std::get<1>(window.imgs)->switch_viewport_state(image_view_state::SYNC);
  }
};

template <unsigned long I>
struct image_viewer_action_handler<
    keybindings::actions::async_viewport_mode<I>> {
  void operator()(const keybindings::actions::async_viewport_mode<I> &,
                  parallel_image_viewer_window &window) const {
    window.perspective = std::visit(
        [](auto &&current_mode) {
          return perspectives::mode_switcher<
              std::decay_t<decltype(current_mode)>,
              perspectives::async_mode<I>>()(current_mode);
        },
        window.perspective);

    std::get<0>(window.imgs)
        ->switch_viewport_state(0 == I ? image_view_state::ACTIVE
                                       : image_view_state::INACTIVE);
    std::get<1>(window.imgs)
        ->switch_viewport_state(1 == I ? image_view_state::ACTIVE
                                       : image_view_state::INACTIVE);
  }
};

template <>
struct image_viewer_action_handler<keybindings::actions::keyboard_help> {
  void operator()(const keybindings::actions::keyboard_help &,
                  parallel_image_viewer_window &window) const {
    window.help_window->Show();
  }
};

void parallel_image_viewer_window::execute_action(
    const keybindings::actions::action &action) {
  std::visit(
      [this](auto &&a) {
        image_viewer_action_handler<std::decay_t<decltype(a)>>()(a, *this);
      },
      action);
}
void parallel_image_viewer_window::update_images(
    filesystem::image_set_sync_iterator *it) {
  auto current_img = it->current_images();
  update_image<0>(current_img);
  update_image<1>(current_img);
}
parallel_image_viewer_window::parallel_image_viewer_window(
    std::unique_ptr<filesystem::image_set> &&imset,
    const keybindings::keyboard_config &keyconfig)
    : wxFrame(nullptr, wxID_ANY, "Parallel image viewer"),
      imset(std::move(imset)) {
  for (auto &&b : keyconfig.bindings) {
    keymap.insert(std::make_pair(b.key, b.action));
  }
  auto sizer = new wxGridSizer(2);
  auto it = this->imset->get_sync_iterator();
  std::generate(imgs.begin(), imgs.end(), [this]() {
    return new image_view(std::unique_ptr<wxBitmap>(), this, wxID_ANY);
  });

  update_images(it.get());

  mode = browsing_mode::sync_mode{std::move(it)};

  std::for_each(imgs.begin(), imgs.end(), [sizer](image_view *img) {
    img->switch_state(image_view_state::SYNC);
    sizer->Add(img, 1, wxEXPAND);
  });

  SetSizer(sizer);
  CreateStatusBar();

  std::get<0>(imgs)->Bind(wxEVT_CHAR, [this](const wxKeyEvent &ev) {
    std::wcout << ev.GetUnicodeKey() << "\n";
    if (decltype(keymap)::const_iterator it;
        (it = keymap.find(ev.GetUnicodeKey())) != keymap.end()) {
      execute_action(it->second);
    }
  });

  std::get<1>(imgs)->Bind(wxEVT_CHAR, [this](const wxKeyEvent &ev) {
    std::wcout << ev.GetUnicodeKey() << "\n";
    if (decltype(keymap)::const_iterator it;
        (it = keymap.find(ev.GetUnicodeKey())) != keymap.end()) {
      execute_action(it->second);
    }
  });
  help_window = new help::keyboard_window(this, wxID_ANY, keyconfig);
  help_window->SetAutoLayout(true);
}
void parallel_image_viewer_window::prev_img() {
  std::visit(
      [this](auto &&mode) {
        std::visit(
            [this](auto &&update) {
              if constexpr (std::tuple_size_v<std::decay_t<decltype(update)>> ==
                            0) {
                SetStatusText("No more images!");
              } else {
                apply_updates(update);
              }
            },
            browsing_mode::mode_ops<std::decay_t<decltype(mode)>>().backward(
                mode));
      },
      mode);
}
void parallel_image_viewer_window::next_img() {
  std::visit(
      [this](auto &&mode) {
        std::visit(
            [this](auto &&update) {
              if constexpr (std::tuple_size_v<std::decay_t<decltype(update)>> ==
                            0) {
                SetStatusText("No more images!");
              } else {
                apply_updates(update);
              }
            },
            browsing_mode::mode_ops<std::decay_t<decltype(mode)>>().forward(
                mode));
      },
      mode);
}
void parallel_image_viewer_window::switch_to_sync_mode() {
  mode = std::visit(
      [this](auto &&m) {
        auto newmode = browsing_mode::mode_switcher<std::decay_t<decltype(m)>,
                                                    browsing_mode::sync_mode>()(
            std::move(m));
        update_images(newmode.iterator.get());
        return newmode;
      },
      mode);
  std::get<0>(imgs)->switch_state(image_view_state::SYNC);
  std::get<1>(imgs)->switch_state(image_view_state::SYNC);
}

template <long unsigned int N>
void parallel_image_viewer_window::update_image_single(
    const std::optional<std::filesystem::path> &path) {
  if (path) {
    wxImage img(wxString(path.value().string()));
    if (img.IsOk()) {
      std::get<N>(this->imgs)->set_image(std::make_unique<wxBitmap>(img), path);
    } else {
      std::get<N>(this->imgs)->set_error("Image loading failed", path);
    }
  } else {
    std::get<N>(this->imgs)->set_image(std::unique_ptr<wxBitmap>(), path);
  }
}

template <class... Args>
void parallel_image_viewer_window::apply_updates(
    const std::tuple<Args...> &updates) {
  std::apply(
      [this](auto &&...args) {
        (this->update_image_single<std::decay_t<decltype(args)>::which>(
             args.new_path),
         ...);
      },
      updates);
}

template <unsigned long Current, unsigned long Active>
void parallel_image_viewer_window::async_mode_for_view() {
  if constexpr (Current == Active) {
    std::get<Current>(imgs)->switch_state(image_view_state::ACTIVE);
  } else {
    std::get<Current>(imgs)->switch_state(image_view_state::INACTIVE);
  }
}

template <long unsigned int N, long unsigned int S>
void parallel_image_viewer_window::update_image(
    const std::array<std::optional<std::filesystem::path>, S> &paths) {
  update_image_single<N>(std::get<N>(paths));
}

template <unsigned long I>
void parallel_image_viewer_window::switch_to_async_mode() {
  mode = std::visit(
      [this](auto &&m) {
        auto newmode =
            browsing_mode::mode_switcher<std::decay_t<decltype(m)>,
                                         browsing_mode::async_mode<I>>()(
                std::move(m));
        update_image_single<0>(std::get<0>(newmode.iterators)->current_image());
        update_image_single<1>(std::get<1>(newmode.iterators)->current_image());
        return newmode;
      },
      mode);
  async_mode_for_view<0, I>();
  async_mode_for_view<1, I>();
}

} // namespace piv::window
