#ifndef KEYBINDINGS_JSON_H
#define KEYBINDINGS_JSON_H

#include <boost/hana/string.hpp>
#include <nlohmann/json.hpp>
#include <variant_json_helper.hpp>
#include <wx/string.h>

#include "keybindings.hpp"

namespace piv::keybindings::actions {
NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(pan<direction::HORIZONTAL>, pixels);
NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(pan<direction::VERTICAL>, pixels);
NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(zoom, amount);
VARIANT_JSON_HELPER_EMPTY_CLASS_NON_INTRUSIVE(next_image);
VARIANT_JSON_HELPER_EMPTY_CLASS_NON_INTRUSIVE(prev_image);
VARIANT_JSON_HELPER_EMPTY_CLASS_NON_INTRUSIVE(async_mode<0>);
VARIANT_JSON_HELPER_EMPTY_CLASS_NON_INTRUSIVE(async_mode<1>);
VARIANT_JSON_HELPER_EMPTY_CLASS_NON_INTRUSIVE(sync_mode);
VARIANT_JSON_HELPER_EMPTY_CLASS_NON_INTRUSIVE(quit);
VARIANT_JSON_HELPER_EMPTY_CLASS_NON_INTRUSIVE(async_viewport_mode<0>);
VARIANT_JSON_HELPER_EMPTY_CLASS_NON_INTRUSIVE(async_viewport_mode<1>);
VARIANT_JSON_HELPER_EMPTY_CLASS_NON_INTRUSIVE(sync_viewport_mode);
VARIANT_JSON_HELPER_EMPTY_CLASS_NON_INTRUSIVE(keyboard_help);
} // namespace piv::keybindings::actions

NLOHMANN_JSON_NAMESPACE_BEGIN

namespace {
auto pan_horizontal_name = BOOST_HANA_STRING("pan-horizontal");
auto pan_vertical_name = BOOST_HANA_STRING("pan-vertical");
auto zoom_name = BOOST_HANA_STRING("zoom");
auto next_image_name = BOOST_HANA_STRING("next-image");
auto prev_image_name = BOOST_HANA_STRING("prev-image");
auto async_mode_0_name = BOOST_HANA_STRING("async-mode(0)");
auto async_mode_1_name = BOOST_HANA_STRING("async-mode(1)");
auto sync_mode_name = BOOST_HANA_STRING("sync-mode");
auto quit_name = BOOST_HANA_STRING("quit");
auto async_viewport_mode_0_name = BOOST_HANA_STRING("async-viewport-mode(0)");
auto async_viewport_mode_1_name = BOOST_HANA_STRING("async-viewport-mode(1)");
auto sync_viewport_mode_name = BOOST_HANA_STRING("sync-viewport-mode");
auto keyboard_help_name = BOOST_HANA_STRING("keyboard-help");
} // namespace

template <>
struct adl_serializer<piv::keybindings::actions::action>
    : variant_json_helper::json_from_to_fun<
          variant_json_helper::variant_member_name<
              piv::keybindings::actions::pan<
                  piv::keybindings::actions::direction::HORIZONTAL>,
              decltype(pan_horizontal_name)>,
          variant_json_helper::variant_member_name<
              piv::keybindings::actions::pan<
                  piv::keybindings::actions::direction::VERTICAL>,
              decltype(pan_vertical_name)>,
          variant_json_helper::variant_member_name<
              piv::keybindings::actions::zoom, decltype(zoom_name)>,
          variant_json_helper::variant_member_name<
              piv::keybindings::actions::next_image, decltype(next_image_name)>,
          variant_json_helper::variant_member_name<
              piv::keybindings::actions::prev_image, decltype(prev_image_name)>,
          variant_json_helper::variant_member_name<
              piv::keybindings::actions::async_mode<0>,
              decltype(async_mode_0_name)>,
          variant_json_helper::variant_member_name<
              piv::keybindings::actions::async_mode<1>,
              decltype(async_mode_1_name)>,
          variant_json_helper::variant_member_name<
              piv::keybindings::actions::sync_mode, decltype(sync_mode_name)>,
          variant_json_helper::variant_member_name<
              piv::keybindings::actions::quit, decltype(quit_name)>,
          variant_json_helper::variant_member_name<
              piv::keybindings::actions::async_viewport_mode<0>,
              decltype(async_viewport_mode_0_name)>,
          variant_json_helper::variant_member_name<
              piv::keybindings::actions::async_viewport_mode<1>,
              decltype(async_viewport_mode_1_name)>,
          variant_json_helper::variant_member_name<
              piv::keybindings::actions::sync_viewport_mode,
              decltype(sync_mode_name)>,
          variant_json_helper::variant_member_name<
              piv::keybindings::actions::keyboard_help,
              decltype(keyboard_help_name)>> {};

template <> struct adl_serializer<piv::keybindings::binding> {
  static void from_json(const json &j, piv::keybindings::binding &binding) {
    j.find("action").value().get_to(binding.action);

    binding.key =
        wxString::FromUTF8(j.find("key").value().get<std::string>())[0];
  }

  static void to_json(json &j, const piv::keybindings::binding &binding) {
    j = {{"action", binding.action}, {"key", wxString(binding.key)}};
  }
};

NLOHMANN_JSON_NAMESPACE_END

namespace piv::keybindings {
NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(keyboard_config, bindings);
}
#endif /* KEYBINDINGS_JSON_H */
