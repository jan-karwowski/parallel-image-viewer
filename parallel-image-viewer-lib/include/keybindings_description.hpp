#include "keybindings.hpp"

#include <sstream>
#include <string>

namespace piv::keybindings::actions {

template <class Action> struct describe {
  std::string operator()(const Action &action) const = delete;
};

template <direction D> struct describe<pan<D>> {
  std::string operator()(const pan<D> &action) const {
    std::stringstream s;
    s << "Pan ";
    if constexpr (D == direction::HORIZONTAL) {
      s << "horizontal ";
    } else {
      s << "vertical ";
    }
    s << action.pixels << "px";

    return s.str();
  }
};

template <> struct describe<zoom> {
  std::string operator()(const zoom &action) const {
    std::stringstream s;
    s << "Zoom by " << action.amount;
    return s.str();
  }
};
template <> struct describe<next_image> {
  std::string operator()(const next_image &action) const {
    return "Next image";
  }
};
template <> struct describe<prev_image> {
  std::string operator()(const prev_image &action) const {
    return "Prev image";
  }
};
template <> struct describe<quit> {
  std::string operator()(const quit &action) const { return "Quit"; }
};
template <unsigned int I> struct describe<async_mode<I>> {
  std::string operator()(const async_mode<I> &action) const {
    std::stringstream s;
    s << "Aync picture position mode with focus on panel " << (I + 1);
    return s.str();
  }
};
template <> struct describe<sync_mode> {
  std::string operator()(const sync_mode &action) const {
    return "Sync picture position mode";
  }
};
template <unsigned long I> struct describe<async_viewport_mode<I>> {
  std::string operator()(const async_viewport_mode<I> &action) const {
    std::stringstream s;
    s << "Aync picture viewport mode with focus on panel " << (I + 1);
    return s.str();
  }
};
template <> struct describe<sync_viewport_mode> {
  std::string operator()(const sync_viewport_mode &action) const {
    return "Sync picture position mode";
  }
};
template <> struct describe<keyboard_help> {
  std::string operator()(const keyboard_help &action) const {
    return "Show current keybindings";
  }
};

} // namespace piv::keybindings::actions
