#ifndef KEYBOARD_HELP_WINDOW_H
#define KEYBOARD_HELP_WINDOW_H

#include <wx/wx.h>
#include <keybindings.hpp>

namespace piv::help {
class keyboard_widget : public wxPanel {

public:
  keyboard_widget(wxWindow* parent, wxWindowID id, const keybindings::keyboard_config& keys);
  
};

class keyboard_window : public wxFrame {
public:
  keyboard_window(wxWindow *parend, wxWindowID id,
                  const keybindings::keyboard_config &keys);
};

}

#endif /* KEYBOARD_HELP_WINDOW_H */
