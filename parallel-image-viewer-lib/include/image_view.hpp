#ifndef IMAGE_VIEW_H
#define IMAGE_VIEW_H

#include <filesystem>
#include <memory>
#include <optional>

#include <variant>
#include <wx/wx.h>

namespace piv {
struct image_perspective {
  image_perspective();

  int centerShiftX;
  int centerShiftY;
  float zoomX;
  float zoomY;
};

enum struct image_view_state { SYNC, INACTIVE, ACTIVE };

class image_view : public wxPanel {
  std::variant<std::unique_ptr<wxBitmap>, wxString> image_or_error;
  image_perspective perspective;
  wxControl *img_view;
  wxStaticText *file_path;
  image_view_state current_state;
  image_view_state current_viewport_state;
  std::optional<std::filesystem::path> current_path;

public:
  image_view(std::unique_ptr<wxBitmap> &&image, wxWindow *parent,
             wxWindowID id);

  wxSize DoGetBestSize() const override;
  void set_perspective(const image_perspective &perspective);
  void switch_state(image_view_state state);
  void switch_viewport_state(image_view_state state);
  void set_image(std::unique_ptr<wxBitmap> &&image,
                 const std::optional<std::filesystem::path> &path);
  void set_error(const wxString &error,
                 const std::optional<std::filesystem::path> &path);

private:
  void update_label();
  void OnPaint(wxPaintEvent &);
};

} // namespace piv

#endif /* IMAGE_VIEW_H */
