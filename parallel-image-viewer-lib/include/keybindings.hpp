#ifndef KEYBINDINGS_HPP
#define KEYBINDINGS_HPP

#include <variant>
#include <vector>
#include <wx/chartype.h>

namespace piv::keybindings::actions {

enum class direction { HORIZONTAL, VERTICAL };

template <direction D> struct pan {
  int pixels;
};

struct zoom {
  double amount;
};

struct next_image {};

struct prev_image {};

struct quit {};

template <unsigned int I> struct async_mode {};

struct sync_mode {};

template <unsigned long I> struct async_viewport_mode {};
struct sync_viewport_mode {};
struct keyboard_help {};

typedef std::variant<pan<direction::HORIZONTAL>, pan<direction::VERTICAL>, zoom,
                     next_image, prev_image, async_mode<0>, async_mode<1>,
                     sync_mode, quit, async_viewport_mode<0>,
                     async_viewport_mode<1>, sync_viewport_mode, keyboard_help>
    action;


}; // namespace piv::keybindings::actions


namespace piv::keybindings {
struct binding {
  actions::action action;
  wxChar key;
};

struct keyboard_config {
  std::vector<binding> bindings;
};

} // namespace piv::keybindings

#endif /* KEYBINDINGS_HPP */
