#ifndef IMAGE_SET_H
#define IMAGE_SET_H

#include <filesystem>
#include <functional>
#include <optional>
#include <set>
#include <string>

namespace piv::filesystem {

struct image_set_async_iterator;

struct image_set_sync_iterator {
  virtual bool forward() = 0;
  virtual bool backward() = 0;
  virtual std::array<std::optional<std::filesystem::path>, 2>
  current_images() = 0;
  virtual std::array<std::unique_ptr<image_set_async_iterator>, 2>
  to_async() = 0;

  virtual ~image_set_sync_iterator();
};

struct image_set_async_iterator {
  virtual bool forward() = 0;
  virtual bool backward() = 0;
  virtual std::optional<std::filesystem::path> current_image() = 0;
  virtual std::unique_ptr<image_set_sync_iterator> to_sync() = 0;

  virtual ~image_set_async_iterator();
};

struct image_set {
  virtual std::unique_ptr<image_set_sync_iterator> get_sync_iterator() = 0;
  virtual std::pair<std::unique_ptr<image_set_async_iterator>,
                    std::unique_ptr<image_set_async_iterator>>
  get_async_iterators() = 0;
  virtual ~image_set();
};

class file_path_matching_image_set : public image_set {
  std::filesystem::path first_dir;
  std::filesystem::path second_dir;

  std::set<std::filesystem::path> filenames_in_both_dirs;

public:
  typedef decltype(filenames_in_both_dirs)::const_iterator set_iterator;
  file_path_matching_image_set(const std::filesystem::path &first_dir,
                               const std::filesystem::path &second_dir,
                               const std::set<std::string> &extensions,
                               bool recursive);

  std::unique_ptr<image_set_sync_iterator> get_sync_iterator() override;
  std::pair<std::unique_ptr<image_set_async_iterator>,
            std::unique_ptr<image_set_async_iterator>>
  get_async_iterators() override;

  virtual ~file_path_matching_image_set();

  friend class file_path_matching_image_set_sync_iterator;
  friend class file_path_matching_image_set_async_iterator;

private:
  std::pair<std::optional<std::filesystem::path>,
            std::optional<std::filesystem::path>>
      image_paths(set_iterator);
};

class file_path_matching_image_set_sync_iterator
    : public image_set_sync_iterator {
  file_path_matching_image_set::set_iterator position;
  file_path_matching_image_set &set;
  file_path_matching_image_set_sync_iterator(
      file_path_matching_image_set &set,
      file_path_matching_image_set::set_iterator position);

public:
  file_path_matching_image_set_sync_iterator(file_path_matching_image_set &set);

  bool forward() override;
  bool backward() override;
  std::array<std::optional<std::filesystem::path>, 2> current_images() override;
  std::array<std::unique_ptr<image_set_async_iterator>, 2> to_async() override;

  friend class file_path_matching_image_set_async_iterator;
};

class file_path_matching_image_set_async_iterator
    : public image_set_async_iterator {
  file_path_matching_image_set::set_iterator position;
  file_path_matching_image_set &set;
  std::function<std::optional<std::filesystem::path>(
      const std::pair<std::optional<std::filesystem::path>,
                      std::optional<std::filesystem::path>> &)>
      img_selector;
  bool forward_to_next_not_null(decltype(position) it);
  bool backward_to_prev_not_null(decltype(position) it);
  file_path_matching_image_set_async_iterator(
      file_path_matching_image_set &set,
      std::function<std::optional<std::filesystem::path>(
          const std::pair<std::optional<std::filesystem::path>,
                          std::optional<std::filesystem::path>> &)>,
      decltype(position) it);

public:
  file_path_matching_image_set_async_iterator(
      file_path_matching_image_set &set,
      std::function<std::optional<std::filesystem::path>(
          const std::pair<std::optional<std::filesystem::path>,
                          std::optional<std::filesystem::path>> &)>);
  bool forward() override;
  bool backward() override;
  std::optional<std::filesystem::path> current_image() override;
  std::unique_ptr<image_set_sync_iterator> to_sync() override;

  virtual ~file_path_matching_image_set_async_iterator();

  friend class file_path_matching_image_set_sync_iterator;
};

} // namespace piv::filesystem

#endif /* IMAGE_SET_H */
