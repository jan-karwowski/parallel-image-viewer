#ifndef PRALLEL_IMAGE_VIEWER_WINDOW_H
#define PRALLEL_IMAGE_VIEWER_WINDOW_H

#include "image_set.hpp"
#include "image_view.hpp"
#include "keybindings.hpp"
#include "keyboard_help_window.hpp"
#include <map>
#include <wx/wx.h>

namespace piv::window {

class parallel_image_viewer_window;
template <class Action> struct image_viewer_action_handler {
  void operator()(const Action &a,
                  parallel_image_viewer_window &window) const = delete;
};

namespace browsing_mode {
struct sync_mode {
  std::unique_ptr<filesystem::image_set_sync_iterator> iterator;
};

template <unsigned long Active>
  requires(Active < 2)
struct async_mode {
  static constexpr unsigned long active = Active;
  std::array<std::unique_ptr<filesystem::image_set_async_iterator>, 2>
      iterators;
};

typedef std::variant<sync_mode, async_mode<0>, async_mode<1>> modes;
} // namespace browsing_mode

namespace perspectives {
struct sync_mode {
  image_perspective perspective;
};

template <unsigned long Active>
  requires(Active < 2)
struct async_mode {
  std::array<image_perspective, 2> perspectives;
};

typedef std::variant<sync_mode, async_mode<0>, async_mode<1>> mode;
} // namespace perspectives

// TODO two-window-mode
class parallel_image_viewer_window : public wxFrame {
  std::array<image_view *, 2> imgs;
  perspectives::mode perspective;

  std::map<wxChar, keybindings::actions::action> keymap;
  std::unique_ptr<filesystem::image_set> imset;
  browsing_mode::modes mode;
  help::keyboard_window* help_window;

  void switch_to_sync_mode();
  template <unsigned long I> void switch_to_async_mode();
  template <unsigned long Current, unsigned long Active>
  void async_mode_for_view();
  template <class... Args>
  void apply_updates(const std::tuple<Args...> &updates);
  void next_img();
  void prev_img();
  template <long unsigned int N>
  void update_image_single(const std::optional<std::filesystem::path> &path);
  template <long unsigned int N, long unsigned int S>
  void update_image(
      const std::array<std::optional<std::filesystem::path>, S> &paths);
  void execute_action(const keybindings::actions::action &action);

public:
  void update_images(filesystem::image_set_sync_iterator *it);
  parallel_image_viewer_window(std::unique_ptr<filesystem::image_set> &&imset,
                               const keybindings::keyboard_config &keyconfig);

  template <class Action> friend struct image_viewer_action_handler;
};

} // namespace piv::window

#endif /* PRALLEL_IMAGE_VIEWER_WINDOW_H */
