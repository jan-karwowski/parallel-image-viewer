#ifndef LIST_IMAGE_SET_H
#define LIST_IMAGE_SET_H

#include "image_set.hpp"

namespace piv::filesystem {
class list_image_set : public image_set {
  std::vector<std::array<std::optional<std::filesystem::path>, 2>> files;

public:
  explicit list_image_set(
      std::vector<std::array<std::optional<std::filesystem::path>, 2>> files);
  explicit list_image_set(std::vector<std::filesystem::path> files1,
                          std::vector<std::filesystem::path> files2);
  std::unique_ptr<image_set_sync_iterator> get_sync_iterator() override;
  std::pair<std::unique_ptr<image_set_async_iterator>,
            std::unique_ptr<image_set_async_iterator>>
  get_async_iterators() override;
};
} // namespace piv::filesystem

#endif /* LIST_IMAGE_SET_H */
