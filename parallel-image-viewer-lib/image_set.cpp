#include "image_set.hpp"

namespace piv::filesystem {

image_set::~image_set() {}
image_set_sync_iterator::~image_set_sync_iterator() {}

template <class dir_iterator>
void browse_files(const std::filesystem::path &path,
                  const std::set<std::string> &extensions,
                  std::set<std::filesystem::path> &output_set) {
  for (auto &&file : dir_iterator(path)) {
    if (extensions.contains(file.path().extension())) {
      output_set.insert(std::filesystem::relative(file.path(), path));
    }
  }
}

file_path_matching_image_set::file_path_matching_image_set(
    const std::filesystem::path &first_dir,
    const std::filesystem::path &second_dir,
    const std::set<std::string> &extensions, bool recursive)
    : first_dir(first_dir), second_dir(second_dir) {
  if (recursive) {
    browse_files<std::filesystem::recursive_directory_iterator>(
        first_dir, extensions, filenames_in_both_dirs);
    browse_files<std::filesystem::recursive_directory_iterator>(
        second_dir, extensions, filenames_in_both_dirs);

  } else {
    browse_files<std::filesystem::directory_iterator>(first_dir, extensions,
                                                      filenames_in_both_dirs);
    browse_files<std::filesystem::directory_iterator>(second_dir, extensions,
                                                      filenames_in_both_dirs);
  }
}

bool file_path_matching_image_set_sync_iterator::forward() {
  auto next = position;
  ++next;
  if (next != set.filenames_in_both_dirs.end()) {
    position = next;
    return true;
  } else {
    return false;
  }
}

file_path_matching_image_set::~file_path_matching_image_set() {}
file_path_matching_image_set_sync_iterator::
    file_path_matching_image_set_sync_iterator(
        file_path_matching_image_set &set)
    : set(set) {
  position = set.filenames_in_both_dirs.begin();
}
bool file_path_matching_image_set_sync_iterator::backward() {
  if (set.filenames_in_both_dirs.begin() == position) {
    return false;
  } else {
    --position;
    return true;
  }
}

std::array<std::optional<std::filesystem::path>, 2>
file_path_matching_image_set_sync_iterator::current_images() {
  if (position != set.filenames_in_both_dirs.end()) {
    std::filesystem::path path1 = set.first_dir / *position;
    std::filesystem::path path2 = set.second_dir / *position;
    return {std::filesystem::is_regular_file(path1) ? std::optional(path1)
                                                    : std::nullopt,
            std::filesystem::is_regular_file(path2) ? std::optional(path2)
                                                    : std::nullopt};
  } else {
    return {std::nullopt, std::nullopt};
  }
}
std::unique_ptr<image_set_sync_iterator>
file_path_matching_image_set::get_sync_iterator() {
  return std::unique_ptr<image_set_sync_iterator>(
      new file_path_matching_image_set_sync_iterator(*this));
}
image_set_async_iterator::~image_set_async_iterator() {}

std::pair<std::unique_ptr<image_set_async_iterator>,
          std::unique_ptr<image_set_async_iterator>>
file_path_matching_image_set::get_async_iterators() {
  return std::make_pair(
      std::make_unique<file_path_matching_image_set_async_iterator>(
          *this, [](auto &&pair) { return pair.first; }),
      std::make_unique<file_path_matching_image_set_async_iterator>(
          *this, [](auto &&pair) { return pair.second; }));
}

file_path_matching_image_set_async_iterator::
    ~file_path_matching_image_set_async_iterator() {}
std::optional<std::filesystem::path>
file_path_matching_image_set_async_iterator::current_image() {
  return img_selector(set.image_paths(position));
}
bool file_path_matching_image_set_async_iterator::backward() {
  if (position == set.filenames_in_both_dirs.begin()) {
    return false;
  }

  auto it = position;
  it--;
  while (it != set.filenames_in_both_dirs.begin() &&
         !img_selector(set.image_paths(it))) {
    --it;
  }
  if (img_selector(set.image_paths(it))) {
    position = it;
    return true;
  } else {
    return false;
  }
}
bool file_path_matching_image_set_async_iterator::forward() {
  auto it = position;
  ++it;
  return forward_to_next_not_null(it);
}
bool file_path_matching_image_set_async_iterator::forward_to_next_not_null(
    decltype(position) it) {
  while (it != set.filenames_in_both_dirs.end() &&
         !img_selector(set.image_paths(it))) {
    ++it;
  }
  if (it != set.filenames_in_both_dirs.end()) {
    position = it;
    return true;
  } else {
    return false;
  }
}
file_path_matching_image_set_async_iterator::
    file_path_matching_image_set_async_iterator(
        file_path_matching_image_set &set,
        std::function<std::optional<std::filesystem::path>(
            const std::pair<std::optional<std::filesystem::path>,
                            std::optional<std::filesystem::path>> &)>
            img_selector)
    : set(set), img_selector(std::move(img_selector)) {
  position = set.filenames_in_both_dirs.begin();
  forward_to_next_not_null(position);
}
std::pair<std::optional<std::filesystem::path>,
          std::optional<std::filesystem::path>>
file_path_matching_image_set::image_paths(set_iterator it) {
  if (it != filenames_in_both_dirs.end()) {
    std::filesystem::path path1 = first_dir / *it;
    std::filesystem::path path2 = second_dir / *it;
    return std::make_pair(
        std::filesystem::is_regular_file(path1) ? std::optional(path1)
                                                : std::nullopt,
        std::filesystem::is_regular_file(path2) ? std::optional(path2)
                                                : std::nullopt);
  } else {
    return std::make_pair(std::nullopt, std::nullopt);
  }
}
std::array<std::unique_ptr<image_set_async_iterator>, 2>
file_path_matching_image_set_sync_iterator::to_async() {
  return {std::unique_ptr<image_set_async_iterator>(
              new file_path_matching_image_set_async_iterator(
                  set, [](auto &&pair) { return pair.first; }, position)),
          std::unique_ptr<image_set_async_iterator>(
              new file_path_matching_image_set_async_iterator(
                  set, [](auto &&pair) { return pair.second; }, position))};
}

std::unique_ptr<image_set_sync_iterator>
file_path_matching_image_set_async_iterator::to_sync() {
  return std::unique_ptr<file_path_matching_image_set_sync_iterator>(
      new file_path_matching_image_set_sync_iterator(set, position));
}

file_path_matching_image_set_sync_iterator::
    file_path_matching_image_set_sync_iterator(
        file_path_matching_image_set &set,
        file_path_matching_image_set::set_iterator position)
    : position(position), set(set) {}
bool file_path_matching_image_set_async_iterator::backward_to_prev_not_null(
    decltype(position) it) {
  while (it != set.filenames_in_both_dirs.begin() &&
         img_selector(set.image_paths(it))) {
    ++it;
  }
  if (img_selector(set.image_paths(it))) {
    position = it;
    return true;
  }
  return false;
}

file_path_matching_image_set_async_iterator::
    file_path_matching_image_set_async_iterator(
        file_path_matching_image_set &set,
        std::function<std::optional<std::filesystem::path>(
            const std::pair<std::optional<std::filesystem::path>,
                            std::optional<std::filesystem::path>> &)>
            selector,
        decltype(position) it)
    : set(set), img_selector(selector), position(it) {
  if (forward_to_next_not_null(it)) {
  } else {
    backward_to_prev_not_null(it);
  }
}
} // namespace piv::filesystem
