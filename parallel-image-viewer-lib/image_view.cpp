#include "image_view.hpp"

namespace piv {

wxSize image_view::DoGetBestSize() const { return wxSize(100, 100); }

void image_view::set_perspective(const image_perspective &perspective) {
  this->perspective = perspective;
  this->Refresh();
}
void image_view::switch_state(image_view_state state) {
  if (state != current_state) {
    current_state = state;
    update_label();
  }
}
void image_view::switch_viewport_state(image_view_state state) {
  if (state != current_viewport_state) {
    current_viewport_state = state;
    update_label();
  }
}

void image_view::set_image(std::unique_ptr<wxBitmap> &&image,
                           const std::optional<std::filesystem::path> &path) {
  current_path = path;
  this->image_or_error = std::move(image);
  img_view->Refresh();
  update_label();
}
void image_view::update_label() {
  wxString text;
  switch (current_state) {
  case image_view_state::SYNC:
    text = "--SYNC(P)--";
    break;
  case image_view_state::INACTIVE:
    text = "--INACTIVE(P)--";
    break;
  case image_view_state::ACTIVE:
    text = "--ACTIVE(P)--";
    break;
  };

  switch (current_viewport_state) {
  case image_view_state::SYNC:
    text += "SYNC(V)--";
    break;
  case image_view_state::INACTIVE:
    text += "INACTIVE(V)--";
    break;
  case image_view_state::ACTIVE:
    text += "ACTIVE--(V)";
    break;
  };

  if (current_path) {
    text += " ";
    text += current_path.value().string();
  } else {
    text += " No file";
  }
  file_path->SetLabel(text);
}
void image_view::OnPaint(wxPaintEvent &) {
  wxPaintDC context(img_view);
  auto width = img_view->GetSize().GetWidth();
  auto height = img_view->GetSize().GetHeight();

  std::visit(
      [&context, width, height, this](auto &&v) {
        typedef std::decay_t<decltype(v)> T;
        if constexpr (std::is_same_v<T, std::unique_ptr<wxBitmap>>) {
          auto &image = v;
          if (!image) {
            context.SetPen(*wxBLACK_PEN);
            auto size = context.GetTextExtent("No image loaded");
            context.DrawText("No image loaded", width / 2 - size.GetWidth() / 2,
                             height / 2 - size.GetHeight() / 2);
          } else {
            wxAffineMatrix2D matrix;
            matrix.Translate(width / 2.0, height / 2.0);
            matrix.Scale(std::exp(perspective.zoomX),
                         std::exp(perspective.zoomY));
            context.SetTransformMatrix(matrix);
            context.DrawBitmap(
                *image, -image->GetWidth() / 2.0 + perspective.centerShiftX,
                -image->GetHeight() / 2.0 + perspective.centerShiftY);
          }
        } else if (std::is_same_v<T, wxString>) {
          context.SetPen(*wxBLACK_PEN);
          auto size = context.GetTextExtent(v);
          context.DrawText(v, width / 2 - size.GetWidth() / 2,
                           height / 2 - size.GetHeight() / 2);
        }
      },
      image_or_error);
}
image_view::image_view(std::unique_ptr<wxBitmap> &&image, wxWindow *parent,
                       wxWindowID id)
    : wxPanel(parent, id), image_or_error(std::move(image)),
      current_state(image_view_state::INACTIVE) {
  img_view = new wxControl(this, wxID_ANY);
  file_path = new wxStaticText(this, wxID_ANY, "TEST");
  img_view->Bind(wxEVT_PAINT, [this](wxPaintEvent &arg) { OnPaint(arg); });
  wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);
  img_view->Bind(wxEVT_CHAR, [](wxKeyEvent &ev) {
    ev.Skip();
    ev.ResumePropagation(1);
  });
  file_path->Bind(wxEVT_CHAR, [](wxKeyEvent &ev) {
    ev.Skip();
    ev.ResumePropagation(1);
  });
  sizer->Add(img_view, 1, wxEXPAND);
  sizer->Add(file_path);
  SetSizer(sizer);
  update_label();
}
image_perspective::image_perspective()
    : centerShiftX(0), centerShiftY(0), zoomX(0), zoomY(0) {}

void image_view::set_error(const wxString &error,
                           const std::optional<std::filesystem::path> &path) {
  image_or_error = error;
  current_path = path;
  img_view->Refresh();
  update_label();
}
} // namespace piv
