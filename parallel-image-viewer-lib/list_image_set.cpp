#include "list_image_set.hpp"

namespace piv::filesystem {

namespace {
typedef std::vector<std::array<std::optional<std::filesystem::path>, 2>> vec;

struct sync_iterator : image_set_sync_iterator {
  const vec &vect;
  vec::const_iterator position;

  sync_iterator(const vec &vect, vec::const_iterator position)
      : vect(vect), position(position) {}

  std::array<std::unique_ptr<image_set_async_iterator>, 2> to_async() override;
  bool forward() override {
    auto it = position;
    ++it;
    if (it != vect.end()) {
      position = it;
      return true;
    } else {
      return false;
    }
  }
  bool backward() override {
    if (position == vect.begin()) {
      return false;
    } else {
      --position;
      return true;
    }
  }
  std::array<std::optional<std::filesystem::path>, 2>
  current_images() override {
    if (position != vect.end()) {
      return *position;
    } else {
      return {std::nullopt, std::nullopt};
    }
  }
  virtual ~sync_iterator() {}
};

template <unsigned long I> struct async_iterator : image_set_async_iterator {
  async_iterator(const vec &vec, vec::const_iterator position)
      : vect(vec), position(position) {
    while (position != vec.begin() && !std::get<I>(*position)) {
      --position;
    }
  }
  const vec &vect;
  vec::const_iterator position;

  std::unique_ptr<image_set_sync_iterator> to_sync() override {
    return std::unique_ptr<image_set_sync_iterator>(
        new sync_iterator(vect, position));
  }
  bool forward() override {
    auto it = position;
    ++it;
    if (it == vect.end() || !std::get<I>(*it)) {
      return false;
    } else {
      position = it;
      return true;
    }
  }
  bool backward() override {
    if (position == vect.begin()) {
      return false;
    } else {
      --position;
      return true;
    }
  }
  std::optional<std::filesystem::path> current_image() override {
    if (position == vect.end()) {
      return std::nullopt;
    } else {
      return std::get<I>(*position);
    }
  }
  virtual ~async_iterator() {}
};

std::array<std::unique_ptr<image_set_async_iterator>, 2>
sync_iterator::to_async() {
  return {std::unique_ptr<image_set_async_iterator>(
              new async_iterator<0>(vect, position)),
          std::unique_ptr<image_set_async_iterator>(
              new async_iterator<1>(vect, position))};
}
} // namespace

std::unique_ptr<image_set_sync_iterator> list_image_set::get_sync_iterator() {
  return std::unique_ptr<image_set_sync_iterator>(
      new sync_iterator(files, files.begin()));
}

std::pair<std::unique_ptr<image_set_async_iterator>,
          std::unique_ptr<image_set_async_iterator>>
list_image_set::get_async_iterators() {
  return {std::unique_ptr<image_set_async_iterator>(
              new async_iterator<0>(files, files.begin())),
          std::unique_ptr<image_set_async_iterator>(
              new async_iterator<1>(files, files.begin()))};
}
list_image_set::list_image_set(
    std::vector<std::array<std::optional<std::filesystem::path>, 2>> files)
    : files(std::move(files)) {}
list_image_set::list_image_set(std::vector<std::filesystem::path> files1,
                               std::vector<std::filesystem::path> files2) {
  files.reserve(std::max(files1.size(), files2.size()));
  auto it1 = files1.begin();
  auto it2 = files2.begin();
  while (it1 != files1.end() && it2 != files2.end()) {
    files.push_back({*it1, *it2});
    ++it1;
    ++it2;
  }

  while (it1 != files1.end()) {
    files.push_back({*it1, std::nullopt});
    ++it1;
  }
  while (it2 != files2.end()) {
    files.push_back({std::nullopt, *it2});
    ++it2;
  }
}
} // namespace piv::filesystem
